package id.ac.tugas.sab.komponenandroid;


import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TampilanFragment extends Fragment {

    static String data;

    public static  TampilanFragment newInstance(String param) {
        // Required empty public constructor
        data = param;
        return new TampilanFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        TextView textView = new TextView(getActivity());
        textView.setText(data);
        return textView;
    }

}
