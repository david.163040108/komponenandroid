package id.ac.tugas.sab.komponenandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin =(Button) findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();
                if (username.trim().equalsIgnoreCase("")) {
                    editTextUsername.setError("Username tidak boleh kosong");
                    editTextUsername.requestFocus();
                }else if (password.trim().equalsIgnoreCase("")){
                    editTextPassword.setError("Passsword tidak boleh kosong");
                    editTextPassword.requestFocus();
                }else{
                    if (username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("name", "Administrator");
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }else if (username.equalsIgnoreCase("unpas") && password.equalsIgnoreCase("sab")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("name","Universitas");
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "Username dan Password tidak sesuai", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
