  package id.ac.tugas.sab.komponenandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

  public class MainActivity extends AppCompatActivity {

    TextView textView;
    ListView listView;
    String arrMenu [] =
            {"1. Activity", "2. Fragmen", "3. LinearLayout", "4. RelativeLayout", "5. Logout"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        listView = (ListView) findViewById(R.id.listView);

        String name = getIntent().getExtras().getString("name");
        textView.setText("Welcome : " + name);
        listView.setAdapter(
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrMenu)
        );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1){
                    startActivity(new Intent(MainActivity.this, MenuFragmenActivity.class));
                }else  if ( position == 2) {
                    startActivity(new Intent(MainActivity.this, LinearLayoutActivity.class));
                }else if (position == 3) {
                    startActivity(new Intent(MainActivity.this, RelativeLayoutActivity.class));
                }else  if (position == 4){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("Apakah Anda Yakin Logout")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    logout();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    public void logout(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

      @Override
      public void onBackPressed() {
          super.onBackPressed();
      }
  }
