package id.ac.tugas.sab.komponenandroid;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuFragmenActivity extends AppCompatActivity {

    Button buttonFragmen_01, buttonFragmen_02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_fragmen);
        buttonFragmen_01 = (Button) findViewById(R.id.buttonFragment_01);
        buttonFragmen_02 = (Button) findViewById(R.id.buttonFragment_02);

    //degaul content
        changeContent(TampilanFragment.newInstance("Fragment 01"));
    buttonFragmen_01.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeContent(TampilanFragment.newInstance("Fragment 01"));
        }
    });

    buttonFragmen_02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(TampilanFragment.newInstance("Fragment 02"));
            }

    });

    }

    public void changeContent(Fragment fragment){
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .commitAllowingStateLoss();
    }
}
